import "./button.css"
export default function FormButton({ content, submit }) {
    return (
        <button
            onClick={() => submit()}
            className="form-submit__btn">{content}</button>
    )
}