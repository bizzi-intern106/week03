import { useLocation, Navigate, Outlet } from "react-router-dom";

export default function ProtectRoute({ allowRoles = [] }) {
    const accessToken = localStorage.getItem("accessToken");
    const location = useLocation();

    let authorized = accessToken ? true : false;

    if (allowRoles.length > 0) {
        authorized = authorized && allowRoles.some(role => role === authState.role);
    }

    console.log(authorized)
    return (
        authorized
            ? <Outlet />
            : <Navigate to='/login' state={{ from: location }} replace />
    )
}
