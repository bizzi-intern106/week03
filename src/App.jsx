import './App.css'
import SignUp from './pages/signup'
import Login from './pages/login'
import { Route, BrowserRouter, Routes } from 'react-router-dom'
import ProtectRoute from './components/protect/protectRouter'
import Home from './pages/home'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/home" element={<ProtectRoute />}>
          <Route index element={<Home />} />
        </Route>
        <Route path='/' element={<SignUp />} />
        <Route path='/signup' element={<SignUp />} />
        <Route path='/login' element={<Login />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
