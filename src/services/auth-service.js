import { devAxios } from "../libs/axios";

export const login = async ({ username, password }) => {
    const response = await devAxios.post("/v1/auth/login", { username, password });
    return response.data;
}

export const signup = async ({ username, password }) => {
    const response = await devAxios.post("/v1/auth/signup", { username, password });
    return response.data;
}